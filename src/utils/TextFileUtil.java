package utils;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * @author Antonio J. Rend�n
 */
public class TextFileUtil {
	/**
	 * Method to count the number of lines of the cache file
	 * @param String textFile
	 * @return int numberOfVideos;
	 */
	public int countLines(String textfile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(textfile));
		int numberOfVideos = 0;
		while ((reader.readLine()) != null) {
			numberOfVideos++;
		}
		
		reader.close();
		return numberOfVideos;
	}

}
