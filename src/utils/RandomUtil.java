package utils;
import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Random;

/**
 * @author Antonio J. Rend�n
 */
public class RandomUtil {
	TextFileUtil tfu  = new TextFileUtil();;
	/**
	 * Method to read a random integer. Used in reading a random line from cache textfile.
	 * @param int min, int max
	 * @return int randomInt
	 */
	private int randomize(int min, int max) {
		Random rand = new Random();
		int randomInt = rand.nextInt((max - min) + 1) + min;

		return randomInt;
	}
	
	/**
	 * Method to play the video randomly chosen from the cache file
	 */
	public void playVideo(String textfile) throws IOException {
		BufferedReader reader = new BufferedReader(new FileReader(textfile));
		int max = tfu.countLines(textfile);
		int randomLine = randomize(1, max);
		int targetVideo = 0;
		String path = null;
		
		while ((path = reader.readLine()) != null) {
			if (++targetVideo == randomLine) {
				Desktop.getDesktop().open(new File(path));
			}
		}
		reader.close();
	}
}