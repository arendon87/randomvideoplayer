package utils;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.apache.commons.io.FileUtils;

/**
 * @author Antonio J. Rend�n
 */
public class CacheUtil {
	private File file;

	/**
	 * Default Constructor
	 */
	public CacheUtil() {
		file = new File("cache.txt");
	}


	public File getFileContents() {
		return file;
	}

	/**
	 * Method to recursively search for movie files in a directory and create a cache textfile with the paths of all the video files
	 * @param File dir
	 */
	public void createCacheFile(File dir) throws IOException {
		try {
			FileWriter cacheFile = new FileWriter("cache.txt");
			PrintWriter printWriter = new PrintWriter(cacheFile);

			String [] fileFormatsArray = new String[] {"avi", "divx", "flv", "mkv", "mov", "mp4", "mpg", "mpeg", "wmv", "webm"};	
			List<File> listOfFiles = (List<File>) FileUtils.listFiles(dir, fileFormatsArray, true);
			for (File file: listOfFiles) {
				if (file.isDirectory()) {
					createCacheFile(file);// recursive call
				}
				else {
					printWriter.println(file.getCanonicalPath());
					System.out.println(file.getCanonicalPath());
				}	
			}
			printWriter.close();
		}
		catch (Exception e) {
			e.printStackTrace();
		}	
	}

}
