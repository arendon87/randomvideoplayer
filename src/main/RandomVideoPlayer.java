package main;
import java.io.File;
import java.io.IOException;

import utils.CacheUtil;
import utils.RandomUtil;


/**
 * Class to randomly play a video file from video files found recursively in directory and sub-directories
 * @author Antonio J. Rend�n
 */
public class RandomVideoPlayer {
	public static void main(String[] args) throws IOException {
		RandomUtil random = new RandomUtil();
		CacheUtil cc = new CacheUtil();
		File file = new File(".");// current directory
		
		try {	
			// look for all the videos and store list them in a text file
			cc.createCacheFile(file);
			// look at the list of videos and play one at random
			random.playVideo("cache.txt");	
		}
		catch (Exception e) {
			System.out.println("Something went wrong...Go check it out!");
		}

	}

}
